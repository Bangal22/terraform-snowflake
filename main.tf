module "warehouses" {
  source = "./modules/warehouses"

  SNOWFLAKE_WAREHOUSES = local.warehouses

  providers = {
    snowflake : snowflake.ACCOUNTADMIN
  }
}

module "databases" {
  source = "./modules/databases"

  SNOWFLAKE_DATABASES = local.databases

  providers = {
    snowflake : snowflake.ACCOUNTADMIN
  }

  depends_on = [module.warehouses]
}

module "schemas" {
  source = "./modules/schemas"

  SNOWFLAKE_SCHEMAS = local.schemas

  providers = {
    snowflake : snowflake.ACCOUNTADMIN
  }

  depends_on = [module.databases]
}