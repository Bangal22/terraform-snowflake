locals {
    build_system_service_roles = flatten([
        for account_role_name, account_role in local.system_service_account_roles : [
            for schema_name, schema in local.schemas : {
                if schema.type != "domain" {
                    name            = "${account_role.name}_${schema.name}"
                    parent          = account_role.parent
                    type_permission = account_role.type_permission
                }
            }
        ]
    ])

    system_service_roles = { for resource in local.build_system_service_roles : resource.name => resource }
}