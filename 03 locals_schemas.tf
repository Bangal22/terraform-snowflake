locals {
  schema_source_oracle_transkal = "ORACLE_TRANSKAL"
  schema_source_salesforce      = "SALESFORCE"
  schema_source_sap             = "SAP"
  schema_source_sql_server      = "SQL_SERVER"

  schema_domain_clients       = "CLIENTS"
  schema_domain_consignaments = "CONSIGNAMENTS"

  source_databases = [local.db_staging, local.db_raw, local.db_business_data_vault]
  domain_databases = [local.db_information_delivery]

  schemas = {
    "${local.schema_source_oracle_transkal}" : {
      name                = local.schema_source_oracle_transkal
      data_retention_days = 3
      is_transient        = false
      is_managed          = false
      databases           = local.source_databases
      type                = "source"
    },
    "${local.schema_source_salesforce}" : {
      name                = local.schema_source_salesforce
      data_retention_days = 3
      is_transient        = false
      is_managed          = false
      databases           = local.source_databases
      type                = "source"
    },
    "${local.schema_source_sap}" : {
      name                = local.schema_source_sap
      data_retention_days = 3
      is_transient        = false
      is_managed          = false
      databases           = local.source_databases
      type                = "source"
    },
    "${local.schema_source_sql_server}" : {
      name                = local.schema_source_sql_server
      data_retention_days = 3
      is_transient        = false
      is_managed          = false
      databases           = local.source_databases
      type                = "source"
    },
    "${local.schema_domain_clients}" : {
      name                = local.schema_domain_clients
      data_retention_days = 3
      is_transient        = false
      is_managed          = false
      databases           = local.domain_databases
      type                = "domain"
    },
    "${local.schema_domain_consignaments}" : {
      name                = local.schema_domain_consignaments
      data_retention_days = 3
      is_transient        = false
      is_managed          = false
      databases           = local.domain_databases
      type                = "domain"
    }
  }
}
