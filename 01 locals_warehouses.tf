locals {
  warehouse_xs = "${local.environment}_WAREHOUSE_XS"
  warehouses = {
    "${local.warehouse_xs}" : {
      name                         = local.warehouse_xs
      comment                      = "XS Warehouse for ${local.environment}"
      auto_suspend                 = 300
      auto_resume                  = true
      size                         = "X-Small"
      min_cluster_count            = 1
      max_cluster_count            = 1
      scaling_policy               = "Economy"
      create_resource_monitor      = false
      initially_suspended          = true
      max_concurrency_level        = 1
      warehouse_type               = "STANDARD"
      statement_timeout_in_seconds = 86400
    }
  }
}
