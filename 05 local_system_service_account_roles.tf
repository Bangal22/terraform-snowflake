locals {
  // CREATE SYSTEM SERVICE ACCOUNT ROLES 
  loader      = "${local.environment}_LOADER"
  reporter    = "${local.environment}_REPORTER"
  transformer = "${local.environment}_TRANSFORMER"

  system_service_account_roles = {
    "${local.loader}" : {
      name    = local.loader
      comment = "${local.environment} loader role"
      type_permission = "all"

      parent = {
        "${local.data_engineer}" : {
          name = local.data_engineer
        }
      }
    },
    "${local.transformer}" : {
      name    = local.transformer
      comment = "${local.environment} transformer role"
      type_permission = "readonly"
      
      parent = {
        "${local.data_engineer}" : {
          name = local.data_engineer
        }
      }
    },
    "${local.reporter}" : {
      name    = local.reporter
      comment = "${local.environment} reporter role"
      type_permission = "readonly"

      parent = {
        "${local.data_analyst}" : {
          name = local.data_analyst
        }
      }
    }
  }
}
