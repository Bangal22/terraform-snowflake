locals {
  db_staging              = "${local.environment}_STAGING"
  db_raw                  = "${local.environment}_RAW"
  db_business_data_vault  = "${local.environment}_BUSINESS_DATA_VAULT"
  db_information_delivery = "${local.environment}_INFORMATION_DELIVERY"

  databases = {
    "${local.db_staging}" : {
      name                        = local.db_staging
      comment                     = "staging"
      data_retention_time_in_days = 3
      is_transient                = false
    },
    "${local.db_raw}" : {
      name                        = local.db_raw
      comment                     = "raw"
      data_retention_time_in_days = 3
      is_transient                = false
    },
    "${local.db_business_data_vault}" : {
      name                        = local.db_business_data_vault
      comment                     = "business data vault"
      data_retention_time_in_days = 3
      is_transient                = false
    },
    "${local.db_information_delivery}" : {
      name                        = local.db_information_delivery
      comment                     = "information delivery"
      data_retention_time_in_days = 3
      is_transient                = false
    }
  }
}
