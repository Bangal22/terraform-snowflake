terraform {
  backend "local" {
    path = "states/dev/terraform.tfstate"
  }
}