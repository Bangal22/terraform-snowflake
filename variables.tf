variable "ENVIRONMENT" {
  description = "environment variable"
  type        = string
}

variable "ACCOUNT" {
  description = "account variable"
  type = string
}

variable "USER" {
  description = "user variable"
  type = string
}

variable "PASSWORD" {
  description = "pasword variable"
  type = string
}

variable "ROLE" {
  description = "rolel variable"
  type = string
}
