provider "snowflake" {
  alias = "ACCOUNTADMIN"
  
  account = var.ACCOUNT
  user = var.USER
  password = var.PASSWORD
  role = var.ROLE
}