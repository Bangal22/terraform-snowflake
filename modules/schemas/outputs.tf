output "created_warehouses" {
  value = snowflake_schema.schemas
  description = "Map of created Snowflake schemas."
}