resource "snowflake_schema" "schemas" {
  for_each = {
    for db in flatten([
      for schema_key, schema in local.schemas : [
        for db in schema.databases : { key = "${db}-${schema_key}", schema = schema, db = db }
      ]
    ]) : db.key => db
  }


  database            = each.value.db
  name                = each.value.schema.name
  data_retention_days = each.value.schema.data_retention_days
  is_transient        = each.value.schema.is_transient
  is_managed          = each.value.schema.is_managed
}
