variable "SNOWFLAKE_SCHEMAS" {
  description = "A map of schemas and their properties"
  type = map(object({
    name                = string
    data_retention_days = number
    is_transient        = bool
    is_managed          = bool
    databases           = list(string)
  }))
}
