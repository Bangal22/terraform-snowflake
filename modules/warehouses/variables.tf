variable "SNOWFLAKE_WAREHOUSES" {
  description = "A map of warehouses and their properties"
  type = map(object({
    name                         = string
    comment                      = string
    auto_suspend                 = number
    auto_resume                  = bool
    size                         = string
    min_cluster_count            = number
    max_cluster_count            = number
    scaling_policy               = string
    create_resource_monitor      = bool
    initially_suspended          = bool
    max_concurrency_level        = number
    warehouse_type               = string
    statement_timeout_in_seconds = number
  }))
}
