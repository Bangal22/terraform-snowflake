
resource "snowflake_warehouse" "warehouse" {
  for_each                     = local.warehouses
  name                         = each.key
  comment                      = each.value.comment
  warehouse_size               = each.value.size
  auto_resume                  = each.value.auto_resume
  auto_suspend                 = each.value.auto_suspend
  initially_suspended          = each.value.initially_suspended
  max_cluster_count            = each.value.max_cluster_count
  min_cluster_count            = each.value.min_cluster_count
  scaling_policy               = each.value.scaling_policy
  max_concurrency_level        = each.value.max_concurrency_level
  warehouse_type               = each.value.warehouse_type
  statement_timeout_in_seconds = each.value.statement_timeout_in_seconds

  lifecycle {
    ignore_changes = all
  }
}