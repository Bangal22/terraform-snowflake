output "created_warehouses" {
  value = snowflake_warehouse.warehouse
  description = "Map of created Snowflake warehouses."
}