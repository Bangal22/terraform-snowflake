output "created_databases" {
  value = snowflake_database.databases
  description = "Map of created Snowflake databases."
}