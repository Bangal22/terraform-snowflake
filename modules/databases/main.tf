resource "snowflake_database" "databases" {
  for_each = local.databases

  name                        = each.value.name
  comment                     = each.value.comment
  data_retention_time_in_days = each.value.data_retention_time_in_days
  is_transient                = each.value.is_transient
}
