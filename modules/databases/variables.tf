variable "SNOWFLAKE_DATABASES" {
  description = "A map of databses and their properties"
  type = map(object({
    name                        = string
    comment                     = string
    data_retention_time_in_days = number
    is_transient                = bool
  }))
}
