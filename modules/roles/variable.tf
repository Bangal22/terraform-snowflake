variable "SNOWFLAKE_CUSTOM_ROLES" {
  description = "A list of custom roles and their properties"
  type = map(object({
    name    = string
    comment = string
  }))
}
