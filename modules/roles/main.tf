resource "snowflake_role" "role" {
  for_each = local.custom_roles

  name    = each.value.name
  comment = each.value.comment
}
