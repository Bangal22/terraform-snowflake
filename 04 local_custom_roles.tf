locals {
  // CREATE CUSTOM ROLES
  data_engineer = "${local.environment}_DATA_ENGINEER"
  data_analyst  = "${local.environment}_DATA_ANALYST"

  custom_roles = {
    "${local.data_engineer}" : {
      name    = local.data_engineer
      comment = "${local.environment} data engineer role"
    },
    "${local.data_analyst}" : {
      name    = local.data_analyst
      comment = "${local.environment} data analyst role"
    }
  }
}
